function GA(cnt, chrom_size, chrom_alphabet, chrom_mutate_chance) {
    let chromosomes = [];

    // === chromosome funcs
    function makeChromo(vals) {
        return { vals: vals, score: null, active: {} };
    }

    function genChromo() {
        const vals = [];
        for (let i=0; i<chrom_size; i+=1) {
            vals.push(parseInt(Math.random()*chrom_alphabet));
        }
        return makeChromo(vals);
    }

    // === main fns
    function mutate(bot_idx, gud_idx) {
        for (let i=0; i<chrom_size; i+=1) {
            chromosomes[bot_idx].vals[i] = (Math.random() < chrom_mutate_chance)
                ? parseInt(Math.random()*chrom_alphabet)
                : chromosomes[gud_idx].vals[i];
        }
    }

    function genesis() {
        chromosomes = [];
        for (let i=0; i<cnt; i+=1) {
            chromosomes.push(genChromo());
        }
    }

    const fake_cull = true;
    function next(cull_ratio) {
        // === no alloc, no real remove
        chromosomes.sort((a,b) => b.score-a.score);
        const scores0 = chromosomes.map(x=>x.score);
        const keep_cnt = parseInt(cull_ratio * cnt);

        // === pick the "type" to generate offspring for (pick best of type)
        const cnt_mutation = cnt - keep_cnt;
        for (let i=0; i<cnt_mutation; i+=1) {
            const bot_idx = keep_cnt+i;
            const gud_idx = parseInt((Math.random()**2)*keep_cnt);
            mutate(bot_idx, gud_idx);
        }
        for (let i=0; i<cnt; i+=1) {
            chromosomes[i].active = {};
        }
        chromosomes.sort((a,b) => 0.5-Math.random());

        const output = [];
        const score_pct = 10;
        for (let i=0; i<score_pct; i+=1) {
            output.push(scores0[parseInt(i*scores0.length/score_pct)]);
        }
        output.push(scores0[scores0.length-1]);
        return (`prev_cnt: ${keep_cnt}, cnt: ${cnt}`
            + `\nscore_pct: ${output.map(x=>parseInt(100*x)/100).join(", ")}`);
    }

    return {
        genesis, next, getChromosomes: () => chromosomes,
    };
}

const botbot1 = (function() {
    const modes = {'LEARNING': 0, 'TEACHER': 1, 'INFERENCE': 2};
    const params = {
        use_teacher: true,
        mode: modes.LEARNING,

        doneratio_gud: 0.5,

        // teacher episilon -- when to activate teacher
        teacher_epi: 0.01,
        teacher_epi_min: 1.0,
        teacher_epi_max: 1.0,

        // teacher-learner params -- only teach when reliable
        teacher_min_pval: 0.01,

        // genetic algo params
        mutate_rate: 0.3,
        cullratio_min: 0.5,
        cullratio_max: 0.8,
        cullratio_scale: 1.5,

        // setting the params for getting the index
        dist_unit: 0.01,
        dist_ratio: [1,10,20],

        // inference params
        inference_minp: 0.05,
    };

    // === visualization
    function drawVisualization() {
        const canvas = document.getElementById('info-canvas');
        const W = Math.ceil(Math.sqrt(chrom_size));
        canvas.width = W;
        canvas.height = 2*W;
        canvas.style.width = global.graph_width;
        canvas.style.height = global.graph_width*3;

        const ctx = canvas.getContext('2d');
        const img = ctx.createImageData(canvas.width, canvas.height);
        const chroms = ga.getChromosomes();

        // === drawing mode action
        function draw_mode_action(oo) {
            for (let i=0; i<chrom_size; i+=1) {
                const x = i%W;
                const y = parseInt(i/W);
                const o = (oo+(x+y*W))*4;

                const v = (Object.keys(inference_vals[i]).map(x=>[x, inference_vals[i][x]])
                    .sort((a,b) => b[1]-a[1])[0][0])/chrom_alphabet;
                img.data[o+0] = 255*(1-(v**2));
                img.data[o+1] = 255*(1-(v**2));
                img.data[o+2] = 255*(1-(v**2));
                img.data[o+3] = 255;
            }
        }

        // === drawing inference dist
        function draw_distribution_diff(oo) {
            const cleng = chroms.length;
            for (let i=0; i<chrom_size; i+=1) {
                const x = i%W;
                const y = parseInt(i/W);
                const o = (oo+(x+y*W))*4;

                let v = 0;
                for (let j=0; j<chrom_alphabet; j+=1) {
                    if (!inference_vals[i][j]) continue;
                    if (inference_vals[i][j]==cleng) continue;
                    const vv = (inference_vals[i][j]/cleng);
                    v -= vv*Math.log(vv);
                }
                v = (1-(v/Math.log(chrom_alphabet)))**2;

                img.data[o+0] = (v*255);
                img.data[o+1] = 0;
                img.data[o+2] = ((1-v)*255);
                img.data[o+3] = 255;
            }
        }

        draw_mode_action(0);
        draw_distribution_diff(W*W);
        ctx.putImageData(img,0,0);
        return img;
    }

    // === actual botbot
    let ga = GA(1);
    function fitness(game) {
        return (
            (-1*game.score.dist_ball_goal) +
            (-0.5*game.score.dist_ball_play) +
            (0.5*game.score.play_ball_touch) +
            (game.score.movement_ball) +
            (game.score.movement_player) +
            (100*game.score.game_done)
        )/game.state.time;
    }
    function getMinEdgeDist(a) { return Math.min(a.x,1-a.x,a.y,1-a.y); }

    // === inference stuff
    let inference_vals = [];
    function updateInference() {
        const empty = [];
        for (let i=0; i<chrom_alphabet; i+=1) { empty.push(0); }

        const chromos = ga.getChromosomes().map(x=>x.vals);
        for (let i=0; i<chrom_size; i+=1) {
            inference_vals[i] = empty.slice();
            for (let j=0; j<chromos.length; j+=1) {
                inference_vals[i][chromos[j][i]] += 1;
            }
        }
    }

    // === main botbot fns
    let params_fns = [];
    let model_cnt = 3;
    let chrom_size = 0;
    let chrom_alphabet = 20;
    function botInit(cnt) {
        // set params fns
        params_fns = [
            (g,b,p) => dist(p,g),
            (g,b,p) => dist(p,b),
            (g,b,p) => dist(b,g),
            (g,b,p) => bearing(p,b),
            (g,b,p) => bearing(p,g),
            (g,b,p) => bearing(b,g),
        ];
        chrom_size = 2*model_cnt*(params_fns.length);

        // set up chroms
        console.log("chrom_size=", chrom_size, "chrom_alphabet=", chrom_alphabet);
        ga = GA(cnt, chrom_size, chrom_alphabet, params.mutate_rate);
        ga.genesis();

        // read from history
        const botbot = JSON.parse(localStorage.getItem('botbot') || '{}');
        if (botbot.size == chrom_size && botbot.alphabet == chrom_alphabet) {
            const chroms = ga.getChromosomes();
            for (let i=0; i<chroms.length; i++) {
                if (!botbot.vals[i]) continue;
                chroms[i].vals = botbot.vals[i];
            }
            console.log("read from history, len=", botbot.vals.length);
        }
        updateInference();

        // ui for botbot
        const div = document.getElementById('controls_bot');
        div.innerHTML = '';

        function elem(tag, text, fn=()=>{}) {
            const ele = document.createElement(tag);
            ele.innerText = text;
            ele.onclick = fn;
            return ele;
        }
        div.appendChild(elem('span', 'mode='));
        div.appendChild(elem('button', 'LEARNING', ()=>params.mode=modes.LEARNING));
        div.appendChild(elem('button', 'INFERENCE', ()=>params.mode=modes.INFERENCE));
    }
    function botNext(games) {
        updateInference();
        drawVisualization();

        const done_ratio = games.filter(x=>x.state.done).length/games.length;
        const chromos = ga.getChromosomes();
        for (let i=0; i<games.length; i+=1) {
            // i wan to have stuff that visited many states
            chromos[i].score = fitness(games[i]);
        }

        const gud_done = params.doneratio_gud;
        const min_c_ratio = params.cullratio_min;
        const max_c_ratio = params.cullratio_max;
        const cull_keepratio = params.cullratio_scale;
        const c_ratio = Math.max(min_c_ratio,Math.min(max_c_ratio,
            Math.min(gud_done, done_ratio)*cull_keepratio));

        function rnd(x) { return Math.round(x*100)/100; }
        $('#info-bot')[0].innerText = JSON.stringify(params) + (
            '\n'+(params.mode==modes.LEARNING ?ga.next(c_ratio) :'')
            + `\nteacher_epi: ${params.use_teacher ?rnd(params.teacher_epi) :0}`
            + `, cull_ratio: ${rnd(c_ratio)}, done_ratio: ${rnd(done_ratio)}`
            + `, mode: ${['LEARNING','TEACHER','INFERENCE'][params.mode]}`
        );

        localStorage.setItem('botbot', JSON.stringify({
            vals: ga.getChromosomes().map(x=>x.vals),
            size: chrom_size, alphabet: chrom_alphabet
        }));
    }
    function botGet(idx) {
        return ga.getChromosomes()[idx];
    }
    function botFunc(game, bot) {
        const vals = bot.vals;
        const goal = game.goal;
        const ball = game.ball;
        const player = game.play;

        // === get model idx
        let best = null;
        let model_idx = 0;
        const params = params_fns.map(x=>x(goal,ball,player));
        for (let i=0; i<model_cnt; i++) {
            let score = 0;
            for (let j=0; j<params.length; j++) {
                score += ((bot.vals[j]/chrom_alphabet)-0.5) * params[j];
            }

            if (best == null || score > best) {
                best = score;
                model_idx = i;
            }
        }
        const offset = (model_cnt+model_idx)*params_fns.length;

        best = null;
        let action = 0;
        for (let i=0; i<8; i++) {
            const angle = i*Math.PI/4;
            const player0 = game_extend(player, player.v, angle);
            const ball0 = isCollide(player,ball) ?game_extend(ball, player.v, angle) :ball;

            let score = 0;
            const params0 = params_fns.map(x=>x(goal,ball0,player0));
            for (let j=0; j<params0.length; j++) {
                score += ((bot.vals[offset+j]/chrom_alphabet)-0.5) * params0[j];
            }

            if (best == null || score > best) {
                best = score;
                action = i;
            }
        }
        return action;
    }


    return { params, modes,
        get: botGet, func: botFunc, next: botNext, init: botInit};
})();
