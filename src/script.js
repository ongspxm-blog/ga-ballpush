const info = {
    puzzleid: 0,
    gameseed: 0,
    frameId: 0,
    trainRound: 0,
    generationId: 0,
    max_done: 0,
};
let doneratios = [];
const info_history = [];
function drawStats() {
    const canvas = document.getElementById('info-gstats');
    const ctx = canvas.getContext('2d');

    const W = 100;
    const H = W/2;
    canvas.width = W;
    canvas.height = H*3;
    canvas.style.width = global.graph_width;
    canvas.style.height = 3*global.graph_width/2;

    const draw_cnt = 60;

    ctx.font = '8px monospace';
    ctx.lineWidth = 1;
    ctx.fillStyle = 'black';
    ctx.strokeStyle = 'black';
    ctx.textBaseline = 'top';

    // === drawing active
    function draw_chart(h, history) {
        const x0 = parseInt((W-draw_cnt)/2);
        const hist_max = Math.max(...history);
        const hist_avg = parseInt(history.reduce((a,b) => a+b, 0)/history.length);

        const margin = 3;
        ctx.textAlign = 'left';
        ctx.fillText(hist_max, 0, h+margin);
        ctx.fillText(parseInt(hist_max/2), 0, h+(H/2)+margin);
        ctx.textAlign = 'right';
        ctx.fillText(hist_avg, W-margin, h+margin);
        ctx.fillText(history[history.length-1], W-margin, h+(H/2)+margin);

        ctx.beginPath();
        for (let i=0; i<draw_cnt; i+=1) {
            ctx.moveTo(x0+i, h+((1-(history[i]/hist_max))*H));
            ctx.lineTo(x0+i, h+H);
            ctx.stroke();
        }
    }

    const maxdone = (info_history.slice(Math.max(0, info_history.length-draw_cnt))
        .map(x=>parseInt(x.max_done*100)));
    if (maxdone.length>1) draw_chart(0, maxdone);

    const history0 = info_history.slice(Math.max(0, info_history.length-draw_cnt)).map(x=>x.tries_taken);
    if (history0.length>1) draw_chart(H, history0);

    const dones = doneratios.slice(Math.max(0, doneratios.length-draw_cnt));
    if (dones.length>1) draw_chart(2*H, dones.map(x=>parseInt(x*100)));
}

let games = [];
let gamesCnt = 100;
function randomizeLocation(d=0) {
    info_history.push({
        max_done: info.max_done,
        tries_taken: info.generationId - info.trainRound,
    });

    const b = global.stage.border;
    function border(x) { return x*(1-2*b)+b; }

    if (d==0) d = (new Date()).getTime();
    global.initial.ball.x = border((d%103)/103);
    global.initial.ball.y = border((d%105)/105);
    global.initial.goal.x = border((d%107)/107);
    global.initial.goal.y = border((d%109)/109);
    global.initial.play.x = border((d%111)/111);
    global.initial.play.y = border((d%113)/113);

    doneratios = [];
    info.max_done = 0;
    info.gameseed = d;
    info.puzzleid += 1;
    info.trainRound = info.generationId;
}

let fps_tstamps = [];
let fps_tstamps0 = -1;
function reset() {
    // computing across games fps
    const num_games = 10
    const gid = info.generationId % num_games;
    fps_tstamps[gid] = (new Date()).getTime();
    if (fps_tstamps[(gid+1)%num_games]) {
        fps_tstamps0 = (1000*global.sim.max_frames*(num_games-1)
            /(fps_tstamps[gid] - fps_tstamps[(gid+1)%num_games]));
    }

    // resetting info
    tstamp0 = -1;
    info.frameId = 0;
    info.generationId += 1;

    const doneratio = games.filter(x=>x.state.done).length/games.length;
    doneratios.push(doneratio);
    if (doneratio > info.max_done) info.max_done = doneratio;

    // NOTE: if everything can touch, make it swap place
    const passing_rate = global.passing_rate;
    const tries = info.generationId - info.trainRound;
    const max_tries = global.max_tries;
    if (doneratio > passing_rate || (tries > max_tries && max_tries > -1)) {
        randomizeLocation();
    }

    const html = [];
    const content = '<div class="goal"></div>'
        + '<div class="play"></div>'
        + '<div class="ball"></div>';

    games = [];
    for (let i=0; i<gamesCnt; i+=1) {
        games.push(genGame(i, info.gameseed));
        html.push(`<div id="game${i}" class="game">${content}</div>`)
    }
    $('#games')[0].innerHTML = html.join('');
}

let timer;
let tstamp0 = -1;
function run() {
    if (timer) clearTimeout(timer);

    const bot = [botbot0, botbot1][global.bot_idx];
    if (games.length == 0) { reset(); bot.init(gamesCnt); }
    $('#info-full')[0].innerText = (`seed: ${info.gameseed}, gen_cnt: ${info.generationId}`
        + `, puzzle: ${info.puzzleid}`
        + `, curr_try: ${info.generationId - info.trainRound}`
        + `, done%: ${parseInt(100*(games.filter(x=>x.state.done).length)/games.length)}`
        + `, fps: ${parseInt(info.frameId*1000/((new Date()).getTime() - tstamp0))}`
        + `, frame: ${info.frameId}`
        + `, fps10: ${parseInt(fps_tstamps0)}`
        + `, max_done: ${parseInt(info.max_done*100)}`
        + `, max_tries: ${global.max_tries}`
    );

    if (info.frameId > global.sim.max_frames) {
        bot.next(games);
        reset();
        drawStats();
    }
    if (tstamp0<0) tstamp0 = (new Date()).getTime();

    // we dun wan to skip too many frames...
    let tstamp_until = tstamp0 + (info.frameId*1000/global.sim.fps);
    for (f=0; f<global.sim.max_frames; f+=1) {
        const skip_drawing = f!=0;
        for (let i=0; i<games.length; i+=1) {
            game_update(games[i], bot.func(games[i], bot.get(i)), $(`#game${i}`)[0], skip_drawing);
            game_updateScore(games[i]);
        }
        info.frameId += 1;
        if (info.frameId > global.sim.max_frames) break;

        tstamp_until += 1000/global.sim.fps;
        if (global.sim.skip_drawing) continue;
        if ((new Date()).getTime() < tstamp_until) break;
    }

    const sleeptime = global.sim.skip_drawing ?0 :(tstamp_until-(new Date()).getTime()-100);
    timer = setTimeout(run, sleeptime);
}

const gamePlayer = genGame();
function runPlayer() {
    $('#gameP')[0].addEventListener('mousemove', onMouseMove);
    global.stage.side = 400;

    update(gamePlayer, playerMove(game));
    timer = setTimeout(runPlayer, 1000/global.sim.fps);
}

function pause() {
    clearTimeout(timer);
}
