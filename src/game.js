const global = {
    actions: 8,
    stage: {
        side:75, buffer:1.1,
        border: 0.1
    },
    size: {
        ball: 0.05,
        goal: 0.02,
        play: 0.1,
    },
    mouse: { x: 0, y:0,
    },
    initial: {
        ball: { x: 0.8, y: 0.5, s: 0.05 },
        goal: { x: 0.2, y: 0.5, s: 0.02 },
        play: { x: 0.9, y: 0.5, s: 0.1, v: 0.01 },
    },
    sim: {
        fps: 120,
        fps_skipframes: 100,
        max_frames: 600,
        skip_drawing: false,
        randomize_pos: true,
    },
    graph_width: 100,
    passing_rate: 0.8,
    max_tries: -1,
    bot_idx: 0
};

function genGame(i, gameseed) {
    function rand(p) {
        if (global.sim.randomize_pos) {
            const c = 100+i;
            p.x = (parseInt(p.x*c)+gameseed)%c/c;
            p.y = (parseInt(p.y*c)+gameseed)%c/c;
        }
        return p;
    }
    return {
        state: { done:false, time:0 },
        ball: rand({ ...global.initial.ball }),
        goal: rand({ ...global.initial.goal }),
        play: rand({ ...global.initial.play }),
    };
}

// === util funcs ===
// NOTE: y points up, x points right
// NOTE: angle goes anti-clockwise, starting from east
function $(css, ele=null) {
    if (!ele) { ele = document; }
    return [].slice.call(ele.querySelectorAll(css));
}

function dist(a, b) {
    return Math.sqrt((a.x-b.x)**2 + (a.y-b.y)**2);
}

function atan(x) {
    // src: https://mazzo.li/posts/vectorized-atan2.html
    const a1  =  0.99997726;
    const a3  = -0.33262347;
    const a5  =  0.19354346;
    const a7  = -0.11643287;
    const a9  =  0.05265332;
    const a11 = -0.01172120;
    const x_sq = x*x;
    return x * (a1 + x_sq * (a3 + x_sq * (a5 + x_sq * (a7 + x_sq * (a9 + x_sq * a11)))));
}

function bearing(a, b) {
    // NOTE: returns angle of b from a
    const dx = b.x - a.x;
    const dy = b.y - a.y;
    const angle = Math.atan(Math.abs(dy/dx));

    if (dy<0) {
        return (dx>0) ? angle : Math.PI-angle;
    } else {
        return (dx<0) ? Math.PI+angle : (2*Math.PI)-angle;
    }
}

function game_extend(pt, dist, angle) {
    // NOTE: given old pt, return new pt
    const dx = Math.abs(Math.cos(angle)*dist);
    const dy = Math.abs(Math.sin(angle)*dist);

    if (angle<Math.PI/2) { return { x: pt.x+dx, y: pt.y-dy }; }
    if (angle<Math.PI) { return { x: pt.x-dx, y: pt.y-dy }; }
    if (angle<Math.PI*3/2) { return { x: pt.x-dx, y: pt.y+dy }; }
    return { x: pt.x+dx, y: pt.y+dy };
}

function isCollide(a, b) {
    // NOTE: assumes a, b are points with {x, y}
    return dist(a, b) < (a.s + b.s)/2;
}

// === mouse funcs
function onMouseMove(evt) {
    global.mouse.x = evt.layerX / global.stage.side;
    global.mouse.y = evt.layerY / global.stage.side;
}

function playerMove(game) {
    return bearing(game.play, global.mouse);
}

// === game funcs
function game_update(game, player_action, gameDiv, skip_draw) {
    // player action definition
    // 0 to 8 = starting from left, 8 directions, clockwise

    function draw(ele, info) {
        ele.style.top = `${(info.y - info.s/2)*100}%`;
        ele.style.left = `${(info.x - info.s/2)*100}%`;

        ele.style.width = `${info.s*100}%`;
        ele.style.paddingBottom = `${info.s*100}%`;
    }

    function checkLimits(x) {
        return Math.min(1.0, Math.max(x, 0.0));
    }

    function game_draw() {
        if (global.sim.skip_drawing) return;
        if (!gameDiv) { gameDiv = $('.game')[0]; }
        draw($('.ball', gameDiv)[0], game.ball);
        draw($('.goal', gameDiv)[0], game.goal);
        draw($('.play', gameDiv)[0], game.play);
        gameDiv.style.width = global.stage.side + 'px';
        gameDiv.style.height = global.stage.side + 'px';
        if (game.state.done) gameDiv.style.opacity = 0.7;
    }

    if (game.state.done) {
        if (!skip_draw) game_draw();
        return game.state.time;
    } else {
        game.state.time += 1;
    }

    // === getting the actions
    let player_angle = 0;
    if (player_action < 8) {
        player_angle = player_action*Math.PI/4;
    }

    // NOTE: moving player based on algo
    const pt = game_extend(game.play, game.play.v, player_angle);
    game.play.x = pt.x; game.play.y = pt.y;

    // NOTE: kick ball away from player
    if (isCollide(game.ball, game.play)) {
        const angle = bearing(game.play, game.ball);
        const dist = (game.play.s + game.ball.s)/2;
        const pt = game_extend(game.play, dist*global.stage.buffer, angle);

        game.ball.x = pt.x;
        game.ball.y = pt.y;
    }

    // NOTE: checking bounds
    game.ball.x = checkLimits(game.ball.x);
    game.ball.y = checkLimits(game.ball.y);
    game.play.x = checkLimits(game.play.x);
    game.play.y = checkLimits(game.play.y);

    // NOTE: end game if game over
    if (isCollide(game.ball, game.goal)) {
        game.state.done = true;
    }

    if (!skip_draw) game_draw();
}

function game_updateScore(game) {
    const ball = game.ball;
    const goal = game.goal;
    const player = game.play;

    // === hackies: ema, used to compute avg ball/player pos
    // so that can minus to see if got moved
    const ema_decay_pframe = 0.8;

    // === some ema ish for scoring
    /*
    return SCORE_DIST_BALL_PLAY * ((game.info.ballDistPlayer/time) * -1)
    + SCORE_DIST_BALL_GOAL * ((game.info.ballDistGoal/time) * -1)
    + SCORE_GAME_TIME * ((time/MAX_FRAME_COUNT) * -1)
    + SCORE_BALL_CONTACT * (game.info.ballContact/time);
    */
    const isdone = game.state.done;
    const emas = {
        ballx: ball.x,
        bally: ball.y,
        playerx: player.x,
        playery: player.y,
    };
    const score = {
        dist_ball_goal: dist(ball,goal),
        dist_ball_play: dist(ball,player),
        play_ball_touch: isCollide(ball,player) ? 1 : 0,
        game_done: isdone ? 1 : 0,
        movement_ball: (isdone || !game.emas) ?1 :dist(ball,{x:game.emas.ballx,y:game.emas.bally}),
        movement_player: (isdone || !game.emas) ?1 :dist(player,{x:game.emas.playerx,y:game.emas.playery}),
    };
    if (!game.score) {
        game.emas = emas;
        game.score = score;
    } else {
        for (const k in score) {
            game.score[k] += score[k];
        }
        for (const k in emas) {
            game.emas[k] = game.emas[k]*ema_decay_pframe + emas[k]*(1-ema_decay_pframe);
        }
    }
}
