function GA(cnt, chrom_size, chrom_alphabet, chrom_mutate_chance) {
    let chromosomes = [];

    // === chromosome funcs
    function makeChromo(vals) {
        return { vals: vals, score: null, active: {} };
    }

    function genChromo() {
        const vals = [];
        for (let i=0; i<chrom_size; i+=1) {
            vals.push(parseInt(Math.random()*chrom_alphabet));
        }
        return makeChromo(vals);
    }

    // === main fns
    function mutate(bot_idx, gud_idx) {
        for (i in chromosomes[gud_idx].active) {
            chromosomes[bot_idx].vals[i] = (Math.random() < chrom_mutate_chance)
                ? parseInt(Math.random()*chrom_alphabet)
                : chromosomes[gud_idx].vals[i];
        }
    }

    function genesis() {
        chromosomes = [];
        for (let i=0; i<cnt; i+=1) {
            chromosomes.push(genChromo());
        }
    }

    const fake_cull = true;
    function next(cull_ratio) {
        // === no alloc, no real remove
        chromosomes.sort((a,b) => b.score-a.score);
        const scores0 = chromosomes.map(x=>x.score);
        const keep_cnt = parseInt(cull_ratio * cnt);

        // === pick the "type" to generate offspring for (pick best of type)
        const cnt_mutation = cnt - keep_cnt;
        for (let i=0; i<cnt_mutation; i+=1) {
            const bot_idx = keep_cnt+i;
            const gud_idx = parseInt((Math.random()**2)*keep_cnt);
            mutate(bot_idx, gud_idx);
        }
        for (let i=0; i<cnt; i+=1) {
            chromosomes[i].active = {};
        }
        chromosomes.sort((a,b) => 0.5-Math.random());

        const output = [];
        const score_pct = 10;
        for (let i=0; i<score_pct; i+=1) {
            output.push(scores0[parseInt(i*scores0.length/score_pct)]);
        }
        output.push(scores0[scores0.length-1]);
        return (`prev_cnt: ${keep_cnt}, cnt: ${cnt}`
            + `\nscore_pct: ${output.map(x=>parseInt(100*x)/100).join(", ")}`);
    }

    return {
        genesis, next, getChromosomes: () => chromosomes,
    };
}

const botbot0 = (function() {
    const modes = {'LEARNING': 0, 'TEACHER': 1, 'INFERENCE': 2};
    const params = {
        use_teacher: true,
        mode: modes.LEARNING,

        doneratio_gud: 0.5,

        // teacher episilon -- when to activate teacher
        teacher_epi: 0.01,
        teacher_epi_min: 1.0,
        teacher_epi_max: 1.0,

        // teacher-learner params -- only teach when reliable
        teacher_min_pval: 0.01,

        // genetic algo params
        mutate_rate: 0.3,
        cullratio_min: 0.5,
        cullratio_max: 0.8,
        cullratio_scale: 1.5,

        // setting the params for getting the index
        dist_unit: 0.01,
        dist_ratio: [1,10,20],

        // inference params
        inference_minp: 0.05,

        // switching between teacher0 and teacher1
        teacher0_pct: 0.5,
    };

    // === visualization
    function drawVisualization() {
        const canvas = document.getElementById('info-canvas');
        const W = Math.ceil(Math.sqrt(chrom_size));
        canvas.width = W;
        canvas.height = 3*W;
        canvas.style.width = global.graph_width;
        canvas.style.height = global.graph_width*3;

        const ctx = canvas.getContext('2d');
        const img = ctx.createImageData(canvas.width, canvas.height);

        // === drawing active
        const active = {};
        const chroms = ga.getChromosomes();
        for (let i=0; i<chroms.length; i+=1) {
            for (j in chroms[i].active) {
                if (!active[j]) { active[j] = 0; }
                active[j] += chroms[i].active[j];
            }
        }
        const maxval = Math.max(...Object.values(active));
        for (i in active) {
            const v = Math.floor(255*(1-active[i]/maxval)**2);
            const y = parseInt(i/W);
            const x = i%W;
            const o = (x+y*W)*4;
            img.data[o+0] = v;
            img.data[o+1] = v;
            img.data[o+2] = v;
            img.data[o+3] = 255;
        }

        // === using hue thingy to determine color
        function clr(v) {
            const clrs = [
                [255, 0, 0], // red
                [255, 255, 0], // yellow
                [0, 255, 0], // green
                [0, 255, 255], // cyan
                [0, 0, 255], // blue
                [255, 0, 255], // magenta
            ];

            const len = clrs.length;
            const idx0 = Math.floor(v*len);
            const idx1 = Math.ceil((v+0.001)*len);
            const val0 = idx0/len;
            const val1 = idx1/len;
            const v0 = (v-val0)/(1/len);
            const v1 = (val1-v)/(1/len);
            return [
                parseInt(clrs[idx0%len][0]*v1+clrs[idx1%len][0]*v0),
                parseInt(clrs[idx0%len][1]*v1+clrs[idx1%len][1]*v0),
                parseInt(clrs[idx0%len][2]*v1+clrs[idx1%len][2]*v0)
            ];
        }

        // === drawing mode action
        function draw_mode_action(oo) {
            for (let i=0; i<chrom_size; i+=1) {
                const x = i%W;
                const y = parseInt(i/W);
                const o = (oo+(x+y*W))*4;

                const v = (Object.keys(inference_vals[i]).map(x=>[x, inference_vals[i][x]])
                    .sort((a,b) => b[1]-a[1])[0][0])/chrom_alphabet;
                const c = clr(v);

                img.data[o+0] = c[0]*(v*0.3 + 0.7);
                img.data[o+1] = c[1]*(v*0.3 + 0.7);
                img.data[o+2] = c[2]*(v*0.3 + 0.7);
                img.data[o+3] = 255;
            }
        }

        // === drawing inference dist
        function draw_distribution_diff(oo) {
            const cleng = chroms.length;
            for (let i=0; i<chrom_size; i+=1) {
                const x = i%W;
                const y = parseInt(i/W);
                const o = (oo+(x+y*W))*4;

                let v = 0;
                for (let j=0; j<chrom_alphabet; j+=1) {
                    if (!inference_vals[i][j]) continue;
                    if (inference_vals[i][j]==cleng) continue;
                    const vv = (inference_vals[i][j]/cleng);
                    v -= vv*Math.log(vv);
                }
                v = (1-(v/Math.log(chrom_alphabet)))**2;

                img.data[o+0] = (v*255);
                img.data[o+1] = 0;
                img.data[o+2] = ((1-v)*255);
                img.data[o+3] = 255;
            }
        }

        draw_mode_action(W*W);
        draw_distribution_diff(W*W*2);
        ctx.putImageData(img,0,0);
        return img;
    }

    // === actual botbot
    let ga = GA(1);
    function sample_noise() {
        return Math.tan(Math.PI*(Math.random()-0.5));
    }
    function fitness(game) {
        return (
            (-1*game.score.dist_ball_goal) +
            (-0.5*game.score.dist_ball_play) +
            (0.5*game.score.play_ball_touch) +
            (game.score.movement_ball) +
            (game.score.movement_player) +
            (100*game.score.game_done)
        )/game.state.time;
    }
    function getMinEdgeDist(a) { return Math.min(a.x,1-a.x,a.y,1-a.y); }
    function getScore(vals,params,offset) {
        var score = 0;
        for (let i=0; i<params.length; i++) {
            score += vals[(offset+i)%vals.length] * params[i];
        }
        return score;
    }

    // item = [(goal, ball, play) => val, [partion0_right, parition1_right, ...]]
    const pi2 = Math.PI*2;
    let params_fns = [];
    function getParams(goal, ball, play) {
        // this is some weirdo multi-base val representation
        let total = 0;
        let repr = 1;
        for (let i=0; i<params_fns.length; i++) {
            const [fn, parts] = params_fns[i];
            const val = fn(goal, ball, play);
            let idx = parts.length;
            for (let j=0; j<parts.length; j++) {
                if (val < parts[j]) {
                    idx = j; break;
                }
            }
            total += idx*repr;
            repr *= (parts.length+1);
        }
        return total;
    }

    // === determining whether to learn from teacher
    let teacher_action = {};
    const facts = [];
    function fact(x) {
        if (facts[x]) return facts[x];
        for (let i=facts.length; i<=x; i+=1) {
            if (i==0 || i==1) { facts[i] = 1; continue; }
            facts[i] = facts[i-1]*i;
        }
        return facts[x];
    }
    function compute_p(o0, o1) {
        // here we are assuming every action is equally likely
        // then we see if the current count we got is actually
        // non-random (not a very high chance of happening)
        const p = 1/chrom_alphabet;
        let c0 = o0;
        let c1 = o1;
        if (o1>150) {
            c1 = 150;
            c0 = Math.round(c1*o0/o1);
        }
        return (fact(c1)/(fact(c0)*fact(c1-c0)))*(p**c0)*((1-p)**(c1-c0));
    }
    function teacher_logic(tid, action) {
        if (!teacher_action[tid]) teacher_action[tid] = {};
        teacher_action[tid][action] = (teacher_action[tid][action] || 0)+1;

        // if not enuf data, then not reliable
        const actions = teacher_action[tid];
        const total = Object.values(actions).reduce((a,b)=>a+b,0);

        // if the actions is too varied also not realiable
        const action0 = Object.keys(actions).sort((a,b) => actions[b]-actions[a])[0];
        const pval = compute_p(actions[action0],total);
        if (pval > params.teacher_min_pval) return null;
        return action0;
    }

    // === teacher algos
    function do_teacher(goal, ball, player) {
        const bearing_err = Math.PI/100;
        const bearing_gb = bearing(goal,ball);
        const bearing_pb = bearing(player,ball);
        const bearing_pg = bearing(player,goal);

        let v_mul, v_angle_fball;

        // some sort of randomization so that dun have jitter
        // but prioritize pushing towards goal
        const dbearing = Math.abs(bearing_pg-bearing_pb);
        let aligned = false;
        if (dbearing < bearing_err) {
            aligned = true;
        } else if (dbearing < bearing_err*10) {
            aligned = Math.random()<(0.3*dbearing/bearing_err);
        }

        // if its alr aligned (all in a line, order correct)
        if (aligned) {
            // move the ball towards goal
            v_mul = 1;
            v_angle_fball = bearing_pb;
        } else {
            // move to the "correct" pusher position
            v_mul = Math.min(20,dist(ball,goal)/0.1);
            v_angle_fball = bearing_gb;
        }

        // if very close to edge, override with some random stuff
        let dd = game_extend(ball, player.v*v_mul, v_angle_fball);
        if (getMinEdgeDist(dd)<0.05) {
            v_angle_fball = Math.random()*pi2;
        }

        dd = game_extend(ball, player.v*v_mul, v_angle_fball);
        return parseInt(bearing(player,dd)/(pi2/8));
    }
    function do_teacher2(goal, ball, player) {
        const gd = dist(goal,player);
        const gx = goal.x-player.x;
        const gy = goal.y-player.y;
        const bd = dist(ball,player);
        const bx = ball.x-player.x;
        const by = ball.y-player.y;

        const balledge = getMinEdgeDist(ball)<0.05;
        const aligned = (bd<0.1) && (bx*gx>0) && (by*gy>0);
        const dx = bx - ((balledge || aligned) ?0: 0.1*gx);
        const dy = by - ((balledge || aligned) ?0: 0.1*gy);

        // this is just mapping action... not real logic
        let action = -1;
        const dr = Math.abs(dy/dx);
        if (dx > 0 && dr<0.5) return 0;
        else if (dx > 0 && dr>0.5 && dr<2 && dy<0) return 1;
        else if (dx > 0 && dr>0.5 && dr<2 && dy>0) return 7;
        else if (dx < 0 && dr<0.5) return 4;
        else if (dx < 0 && dr>0.5 && dr<2 && dy<0) return 3;
        else if (dx < 0 && dr>0.5 && dr<2 && dy>0) return 5;
        else if (dy < 0) return 2;
        else if (dy > 0) return 6;
    }

    // === inference stuff
    let inference_vals = [];
    function updateInference() {
        const empty = [];
        for (let i=0; i<chrom_alphabet; i+=1) { empty.push(0); }

        const chromos = ga.getChromosomes().map(x=>x.vals);
        for (let i=0; i<chrom_size; i+=1) {
            inference_vals[i] = empty.slice();
            for (let j=0; j<chromos.length; j+=1) {
                inference_vals[i][chromos[j][i]] += 1;
            }
        }
    }

    // === main botbot fns
    let chrom_size = 0;
    let chrom_alphabet = 0;
    function botInit(cnt) {
        const pi2 = Math.PI*2;
        const portions = params.dist_ratio.map(x=>x*params.dist_unit);
        params_fns = [
            /*
            [(g,b,p)=>p.x-g.x, [0]],
            [(g,b,p)=>Math.abs(p.x-g.x), portions],
            [(g,b,p)=>p.y-g.y, [0]],
            [(g,b,p)=>Math.abs(p.y-g.y), portions],
            [(g,b,p)=>p.x-b.x, [0]],
            [(g,b,p)=>Math.abs(p.x-b.x), portions],
            [(g,b,p)=>p.y-b.y, [0]],
            [(g,b,p)=>Math.abs(p.y-b.y), portions],
            [(g,b,p)=>g.x-b.x, [0]],
            [(g,b,p)=>Math.abs(g.x-b.x), portions],
            [(g,b,p)=>g.y-b.y, [0]],
            [(g,b,p)=>Math.abs(g.y-b.y), portions],
            */
            [(g,b,p)=>p.x-g.x, [0]],
            [(g,b,p)=>p.y-g.y, [0]],
            [(g,b,p)=>p.x-b.x, [0]],
            [(g,b,p)=>p.y-b.y, [0]],
            [(g,b,p)=>b.x-g.x, [0]],
            [(g,b,p)=>b.y-g.y, [0]],
            [(g,b,p)=>bearing(p,b)-bearing(p,g), [0]],
            [(g,b,p)=>dist(p,b)/dist(p,g), [0.5,1]],
            [(g,b,p)=>dist(p,b)-dist(p,g), [0]],
            /*
            [(g,b,p)=>bearing(p,b)/(pi2/8), [1,2,3,4,5,6,7]],
            [(g,b,p)=>bearing(p,g)/(pi2/8), [1,2,3,4,5,6,7]],
            [(g,b,p)=>dist(p,b)/dist(p,g), [0.5,1,2]],
            [(g,b,p)=>getMinEdgeDist(b), [0.05]],
            [(g,b,p)=>((bearing(p,b)-bearing(p,g)+pi2)%pi2)/(pi2/8), []],
            */
        ];
        chrom_alphabet = global.actions;
        chrom_size = params_fns.reduce((a,b)=>a*(b[1].length+1),1);
        console.log("chrom_size=", chrom_size, "chrom_alphabet=", chrom_alphabet);

        ga = GA(cnt, chrom_size, chrom_alphabet, params.mutate_rate);
        ga.genesis();

        // read from history
        const botbot = JSON.parse(localStorage.getItem('botbot') || '{}');
        if (botbot.size == chrom_size && botbot.alphabet == chrom_alphabet) {
            const chroms = ga.getChromosomes();
            for (let i=0; i<chroms.length; i++) {
                if (!botbot.vals[i]) continue;
                chroms[i].vals = botbot.vals[i];
            }
            console.log("read from history, len=", botbot.vals.length);
        }
        updateInference();

        // ui for botbot
        const div = document.getElementById('controls_bot');
        div.innerHTML = '';

        function elem(tag, text, fn=()=>{}) {
            const ele = document.createElement(tag);
            ele.innerText = text;
            ele.onclick = fn;
            return ele;
        }
        div.appendChild(elem('span', '| teacher0_pct='));
        const input = elem('input', '');
        input.type = 'number';
        input.min = 0;
        input.max = 1;
        input.step = 0.1;
        input.value = params.teacher0_pct;
        input.onchange = function (ele) {
            params.teacher0_pct=this.value;
        };
        div.appendChild(input);

        div.appendChild(elem('span', '| use_teacher='));
        div.appendChild(elem('button', 'toggle', ()=>params.use_teacher=!params.use_teacher));
        div.appendChild(elem('span', '| mode='));
        div.appendChild(elem('button', 'LEARNING', ()=>params.mode=modes.LEARNING));
        div.appendChild(elem('button', 'TEACHER', ()=>params.mode=modes.TEACHER));
        div.appendChild(elem('button', 'INFERENCE', ()=>params.mode=modes.INFERENCE));
    }
    function botNext(games) {
        updateInference();
        drawVisualization();

        const done_ratio = games.filter(x=>x.state.done).length/games.length;
        const chromos = ga.getChromosomes();
        for (let i=0; i<games.length; i+=1) {
            // i wan to have stuff that visited many states
            chromos[i].score = fitness(games[i]);
        }

        // === set teacher epislon + reset
        teacher_action = {};
        const gud_done = params.doneratio_gud;
        const min_t_eps = params.teacher_epi_min;
        const max_t_eps = params.teacher_epi_max;
        params.teacher_epi = max_t_eps - (max_t_eps-min_t_eps) * Math.min(1, done_ratio/gud_done);

        const min_c_ratio = params.cullratio_min;
        const max_c_ratio = params.cullratio_max;
        const cull_keepratio = params.cullratio_scale;
        const c_ratio = Math.max(min_c_ratio,Math.min(max_c_ratio,
            Math.min(gud_done, done_ratio)*cull_keepratio));

        function rnd(x) { return Math.round(x*100)/100; }
        $('#info-bot')[0].innerText = JSON.stringify(params) + (
            '\n'+params.mode==modes.LEARNING ?ga.next(c_ratio) :''
            + `\nteacher_epi: ${params.use_teacher ?rnd(params.teacher_epi) :0}`
            + `, cull_ratio: ${rnd(c_ratio)}, done_ratio: ${rnd(done_ratio)}`
            + `, mode: ${['LEARNING','TEACHER','INFERENCE'][params.mode]}`
        );

        localStorage.setItem('botbot', JSON.stringify({
            vals: ga.getChromosomes().map(x=>x.vals),
            size: chrom_size, alphabet: chrom_alphabet
        }));
    }
    function botGet(idx) {
        return ga.getChromosomes()[idx];
    }
    function botFunc(game, bot) {
        const vals = bot.vals;
        const goal = game.goal;
        const ball = game.ball;
        const player = game.play;

        // add back stats to the bot so that know which one to work on
        // this is like gradient descent, but discrete ish
        const dirs_idx = getParams(goal, ball, player);
        if (!bot.active[dirs_idx]) bot.active[dirs_idx] = 0;
        bot.active[dirs_idx] += 1;

        if (params.mode==modes.INFERENCE) {
            // doing a min-p sampling
            const vals = inference_vals[dirs_idx]
            const total = vals.reduce((a,b)=>a+b,0);

            const cc = [];
            for (let i=0; i<vals.length; i++) {
                if (vals[i]/total < params.inference_minp) continue;
                cc.push([vals[i],i]);
            }

            let cc0 = 0;
            const rr = Math.random();
            const cc1 = cc.reduce((a,b)=>a+b,0);
            for (let i=0; i<cc.length; i++) {
                cc0 += cc[i][0];
                if (rr<(cc0/cc1)) {
                    return cc[i][1];
                }
            }
            return cc[cc.length-1][1];
        }

        // === teacher logic
        let teacher_action = -1;
        let use_teacher = params.use_teacher && Math.random()<params.teacher_epi;
        if (params.mode==modes.TEACHER) use_teacher = true;

        if (use_teacher){
            const teacher_fn = Math.random()<0.3 ?do_teacher :do_teacher2;
            teacher_action = teacher_fn(goal,ball,player);
            if (params.mode==modes.TEACHER) return teacher_action;
        }

        // === learning from teacher
        if (params.mode==modes.LEARNING && teacher_action != -1) {
            const learnt = teacher_logic(dirs_idx, teacher_action);
            if (learnt == teacher_action) bot.vals[dirs_idx] = learnt;
            // if (Math.random() < 0.3) return teacher_action;
        }
        return bot.vals[dirs_idx];
    }


    return { params, modes,
        get: botGet, func: botFunc, next: botNext, init: botInit};
})();
